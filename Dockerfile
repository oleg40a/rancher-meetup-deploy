FROM python:2

WORKDIR /usr/src/app
ADD requirements.txt /usr/src/app/requirements.txt
RUN pip install -r requirements.txt
ADD app/deploy.py /usr/bin/
RUN chmod 755 /usr/bin/deploy.py

CMD ["sh"]